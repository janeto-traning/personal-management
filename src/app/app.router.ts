import { NgModule } from '@angular/core';
import { RouterModule, Route, ExtraOptions } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Route[] = [
    { path: 'login', component: LoginComponent },
    { path: 'layout', loadChildren: 'app/layout/layout.module#LayoutModule' },
    { path: '**', component: PageNotFoundComponent }
];

const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
