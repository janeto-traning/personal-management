import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  switch(tab) {
    let router = '';
    if (tab.index === 0) {
      router = 'home';
    }
    if (tab.index === 1) {
      router = 'diary';
    }
    if (tab.index === 2) {
      router = 'spend';
    }
    this.router.navigate(['layout', router]);
  }
}
