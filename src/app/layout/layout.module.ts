import { HomeComponent } from './home/home.component';
import { MatTabsModule } from '@angular/material/tabs';
import { LayoutRoutingModule } from './layout.routing';
import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { InstallComponent } from './install/install.component';
import { AccountManagementComponent } from './account-management/account-management.component';
import { HelpedComponent } from './helped/helped.component';
import { HeaderComponent } from './header/header.component';
import { SideBarMenuComponent } from './side-bar-menu/side-bar-menu.component';
import { RightMenuBarComponent } from './right-menu-bar/right-menu-bar.component';
import { DiaryComponent } from './diary/diary.component';
import { SpendComponent } from './spend/spend.component';

@NgModule({
    imports: [
        LayoutRoutingModule,
        MatTabsModule
    ],
    exports: [],
    declarations: [
        SideBarMenuComponent,
        RightMenuBarComponent,
        LayoutComponent,
        HeaderComponent,
        HelpedComponent,
        InstallComponent,
        AccountManagementComponent,
        HomeComponent,
        DiaryComponent,
        SpendComponent,
    ],
    providers: [],
})
export class LayoutModule { }
