import { SpendComponent } from './spend/spend.component';
import { DiaryComponent } from './diary/diary.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { InstallComponent } from './install/install.component';
import { HelpedComponent } from './helped/helped.component';
import { AccountManagementComponent } from './account-management/account-management.component';
import { NgModule, Component } from '@angular/core';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent, children: [
            { path: 'home', component: HomeComponent},
            { path: 'diary', component: DiaryComponent},
            { path: 'spend', component: SpendComponent},
            { path: 'helped', component: HelpedComponent},
            { path: 'install', component: InstallComponent},
            { path: 'accout-management', component: AccountManagementComponent},
            { path: 'install', component: InstallComponent},
            { path: 'helped', component: HelpedComponent},
            { path: '', redirectTo: 'home', pathMatch: 'full' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
